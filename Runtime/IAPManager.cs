using System;
using System.Threading.Tasks;
using UnityEngine.Purchasing;

namespace UMNP.GamedevKit.IAP
{
    public static class IAPManager
    {
        private static BaseStoreListener _storeListener;

        public static bool IsInitialized => (_storeListener != null) ? _storeListener.IsInitialized : false;

        public static Product GetProduct(in string id)
        {
            if (!IsInitialized) return null;

#if UNITY_EDITOR
            if (string.IsNullOrEmpty(id))
            {
                Utils.Logging.LogError("ProductId you are passing is empty!");
                return null;
            }
#endif
            return _storeListener.GetProduct(id);
        }

        /// <param name="timeout">timeout in ms</param>
        public async static Task<Structures.Status> InitializeAsync(BaseStoreListener storeListener, int timeout = 10000)
        {
            if (IsInitialized) return Structures.Status.Success;

            _storeListener = storeListener;

            var initializationTask = WaitForInitializationAsync();
            var timeoutTask = Task.Delay(timeout);
            var completedTask = await Task.WhenAny(initializationTask, timeoutTask);

            if (completedTask == timeoutTask)
                return new Structures.Status(false, "Initialization timed out. Try checking your internet connection.");

            return Structures.Status.Success;

            async Task WaitForInitializationAsync()
            {
                while (!_storeListener.IsInitialized)
                    await Task.Yield();
            }
        }

        public static bool IsProductPurchased(in string id)
        {
#if UNITY_EDITOR
            if (string.IsNullOrEmpty(id))
            {
                Utils.Logging.LogError("ProductId you are passing is empty!");
                return false;
            }
#endif
            return _storeListener.IsProductPurchased(id);
        }

        /// <param name="onPurchase">Only for cleanup, actual callback is set in the store listener.</param>
        public static void PurchaseProduct(in string id, in Action onPurchase = null)
        {
            if (!IsInitialized) return;
            _storeListener.PurchaseProduct(id, onPurchase);
        }
    }
}
