using System;
using System.Collections.Generic;
using UnityEngine.Purchasing;

namespace UMNP.GamedevKit.IAP
{
    public abstract class BaseStoreListener : IStoreListener
    {
        protected Dictionary<string, Action> _productPurchaseCallbacks = new Dictionary<string, Action>();

        private IStoreController _storeController;
        private IExtensionProvider _extensionProvider;
        private Action _onPurchase;

        public bool IsInitialized => (_storeController != null) && (_extensionProvider != null);

        public BaseStoreListener(in IAPCatalogue catalogue)
        {
            BuildPurchaseCallbacks();

            var builder = ConfigurationBuilder.Instance(StandardPurchasingModule.Instance());
            foreach (var product in catalogue.Products)
                builder.AddProduct(product.id, product.type);

            UnityPurchasing.Initialize(this, builder);
        }

        public Product GetProduct(in string id) => _storeController.products.WithID(id);
        public bool IsProductPurchased(in string id) => GetProduct(id).hasReceipt;

        public void OnInitialized(IStoreController controller, IExtensionProvider extensions)
        {
            _storeController = controller;
            _extensionProvider = extensions;
        }

        public abstract void OnInitializeFailed(InitializationFailureReason error);

        public virtual void OnPurchaseSuccessful(Product product)
        {
            var id = product.definition.id;
            if (!_productPurchaseCallbacks.ContainsKey(id))
            {
                Utils.Logging.LogError($"Unknown product id: {id}");
                return;
            }

            _productPurchaseCallbacks[id]?.Invoke();
        }

        public abstract void OnPurchaseFailed(Product product, PurchaseFailureReason failureReason);

        public PurchaseProcessingResult ProcessPurchase(PurchaseEventArgs purchaseEvent)
        {
            OnPurchaseSuccessful(purchaseEvent.purchasedProduct);
            _onPurchase?.Invoke();
            _onPurchase = null;
            return PurchaseProcessingResult.Complete;
        }

        public void PurchaseProduct(in string id, in Action onPurchase = null)
        {
            if (!IsInitialized)
            {
                OnPurchaseFailed(null, PurchaseFailureReason.PurchasingUnavailable);
                return;
            }

            _onPurchase = onPurchase;
            _storeController.InitiatePurchase(id);
        }

        /// <summary>Add entries to productPurchaseCallbacks. Key is product id, value is the purchase success callback</summary>
        protected abstract void BuildPurchaseCallbacks();
    }
}
