using System;
using UnityEngine;
using UnityEngine.Purchasing;

namespace UMNP.GamedevKit.IAP
{
    [CreateAssetMenu(fileName = "New IAPCatalogue", menuName = "GamedevKit/IAPCatalogue")]
    public class IAPCatalogue : ScriptableObject
    {
        [SerializeField] private Product[] _products = new Product[] { };

        public Product[] Products => _products;

        [Serializable]
        public struct Product
        {
            public string id;
            public ProductType type;
        }
    }
}