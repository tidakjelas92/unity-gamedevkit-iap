using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace UMNP.GamedevKit.IAP
{
    [RequireComponent(typeof(Button))]
    public class IAPButton : MonoBehaviour
    {
        [Header("References")]
        [SerializeField, Tooltip("Optional")] private TextMeshProUGUI _titleText = null;
        [SerializeField, Tooltip("Optional")] private TextMeshProUGUI _descriptionText = null;
        [SerializeField, Tooltip("Optional")] private TextMeshProUGUI _priceText = null;

        [Header("Parameters")]
        [SerializeField] private string _productId = string.Empty;

        public void Initialize()
        {
            if (!IAPManager.IsInitialized) return;

            var product = IAPManager.GetProduct(_productId);
            if (product == null) return;

            _titleText?.SetText(product.metadata.localizedTitle);
            _descriptionText?.SetText(product.metadata.localizedDescription);
            _priceText?.SetText(product.metadata.localizedPriceString);
        }

        public void PurchaseProduct() => IAPManager.PurchaseProduct(_productId);
        public void SetProductId(in string productId) => _productId = productId;
    }
}
