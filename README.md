# unity-gamedevkit-iap

In app purchase framework.

## How to use

1. Import this package from the package manager, resolve all dependencies.
2. Inherit from BaseStoreListener to create project specific StoreListener.
3. Implement BuildPurchaseCallbacks by adding product id as key and callback as value.
4. Add IAPButton to the purchase button in the UI.
5. IAPButton.\_productId can be set in the prefab or by using IAPButton.SetProductId function.
6. Instantiate the new StoreListener, pass it to IAPManager.InitializeAsync to Initialize the Manager
7. The button should work as intended.
